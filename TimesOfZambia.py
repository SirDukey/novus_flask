from selenium import webdriver
from time import sleep
from PIL import Image
from datetime import datetime
import os
import shutil


def start():

    start_time = str(datetime.now().strftime('%d-%m-%Y %H:%M:%S'))
    print('#========| NOVUS GROUP (PTY)LTD |========#')
    print('#                                        #')
    print('# Scraping tool for the java application #')
    print('# used on "Times of Zambia"              #')
    print('# - paste in the publication url         #')
    print('# - specify the total pages              #')
    print('#                                        #')
    print('#========================================#')
    print()

    # sysargs to populate the url & pages variables
    #url = input('What is the url to scrape: ')
    url = 'http://www.timesepaper.com/index.php'
    pages = 2
    #pages = int(input('How many pages to scrape: '))
    print()
    print('start time:', start_time)
    print('starting browser')
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--log-level=3')
    #chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox') # required when running as root user
    browser = webdriver.Chrome(executable_path='/usr/bin/chromedriver',
                               chrome_options=chrome_options)

    print('scraping url:', url)
    print('attempting to scrape {} pages'.format(pages))
    browser.get(url)
    browser.set_window_size(2000, 2000)
    sleep(5)

    print('logging in')
    username = browser.find_element_by_css_selector('#email')
    username.send_keys('elriza@novusgroup.co.za')
    password = browser.find_element_by_css_selector('#password')
    password.send_keys('NovusGroup12345')
    sleep(5)

    submit = browser.find_element_by_css_selector('#loginBtn > input:nth-child(1)')
    submit.click()
    print('login successful')
    sleep(5)

    read = browser.find_element_by_css_selector('a.paperAction')
    read.click()

    sleep(30)
    browser.quit()


if __name__ == '__main__':
    start()