from selenium import webdriver
from time import sleep
from PIL import Image
from datetime import datetime
from shutil import move
from zipfile import ZipFile
from os import listdir, unlink, makedirs
from os.path import basename


def start_scrape(name, url, pages):

    workingDir = name + '__' + str(datetime.now().strftime('%d-%m-%Y__%H:%M:%S'))
    start_time = workingDir
    makedirs('/Novus_flask/downloaded/' + workingDir)

    try:
        yield 'start time: ' + start_time + '<br/>\n'
        yield 'starting browser<br/>\n'
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--log-level=3')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        browser = webdriver.Chrome(executable_path='/usr/bin/chromedriver', chrome_options=chrome_options)
        browser.set_window_size(1920, 1080)

        yield 'scraping url: ' + url + '<br/>\n'
        try:
            browser.get('https://www.magzter.com/loginBeta')
            # browser.set_window_size(2000, 2000)
        except Exception as e:
            yield str(e) + '<br/>\n'

        yield 'logging in...<br/>\n'
        sleep(2)
        try:
            mailbtn = browser.find_element_by_class_name('mailicon')
            mailbtn.click()
        except Exception as e:
            yield str(e) + '<br/>\n'

        yield 'username...<br/>\n'
        try:
            username = browser.find_element_by_id('emailval')
            username.send_keys('leon@novusgroup.co.za')
        except Exception as e:
            yield str(e) + '<br/>\n'

        try:
            continueBtn = browser.find_element_by_class_name('continuebtn')
            continueBtn.click()
        except Exception as e:
            yield str(e) + '<br/>\n'

        sleep(5)

        yield 'password...<br/>\n'
        try:
            password = browser.find_element_by_id('usrpassword')
            password.send_keys('NovusGroup12345')
        except Exception as e:
            yield str(e) + '<br/>\n'

        try:
            submit = browser.find_element_by_class_name('continuebtn')
            submit.click()
            yield 'login successful!<br/>\n'
            sleep(10)
        except Exception as e:
            yield str(e) + '<br/>\n'

        yield 'opening publication...</br>\n'
        try:
            browser.get(url)
            sleep(2)
        except Exception as e:
            yield str(e) + '<br/>\n'

        yield 'starting loop</br>\n'
        '''==| loop section |=='''
        yield 'number of pages: ' + str(pages) + '</br>\n'
        for i in range(0, int(pages) + 1):
            if i % 2 == 0:
                yield 'capturing page{}...</br>\n'.format(i)
                try:
                    browser.save_screenshot('/Novus_flask/downloaded/{}/page{}.png'.format(workingDir, i))
                    im = Image.open('/Novus_flask/downloaded/{}/page{}.png'.format(workingDir, i))
                    # im.thumbnail((2000, 2000))
                    im = im.convert('RGB')
                    im.save('/Novus_flask/downloaded/{}/page{}.jpeg'.format(workingDir, i))
                    unlink('/Novus_flask/downloaded/{}/page{}.png'.format(workingDir, i))
                    original = Image.open('/Novus_flask/downloaded/{}/page{}.jpeg'.format(workingDir, i))
                    # width, height = original.size
                    # print('original', width)

                    # Crop page edges
                    left = 140
                    top = 0
                    right = 1780
                    bottom = 1076
                    cropped = original.crop((left, top, right, bottom))
                    cropped.save('/Novus_flask/downloaded/{}/page{}.jpeg'.format(workingDir, i))

                    # Split page down the center and rename right page as odd number
                    im = Image.open('/Novus_flask/downloaded/{}/page{}.jpeg'.format(workingDir, i))
                    #width, height = im.size

                    # Left image
                    leftA = 0
                    topA = 0
                    rightA = 820
                    bottomA = 1076
                    croppedA = im.crop((leftA, topA, rightA, bottomA))
                    croppedA.save('/Novus_flask/downloaded/{}/page{}.jpeg'.format(workingDir, i))

                    # Right image
                    leftB = 820
                    topB = 0
                    rightB = 1640
                    bottomB = 1076
                    croppedB = im.crop((leftB, topB, rightB, bottomB))
                    iOdd = i + 1
                    croppedB.save('/Novus_flask/downloaded/{}/page{}.jpeg'.format(workingDir, iOdd))

                except Exception as e:
                    yield str(e) + '<br/>\n'

                yield 'next page...</br>\n'
                try:
                    nextBtn = browser.find_element_by_class_name('next-button')
                    nextBtn.click()
                except Exception as e:
                    yield str(e) + '<br/>\n'
                sleep(2)

        '''==| end of loop section |=='''

        # Join page0 and page1
        first_im = Image.open('/Novus_flask/downloaded/{}/page0.jpeg'.format(workingDir))
        second_im = Image.open('/Novus_flask/downloaded/{}/page1.jpeg'.format(workingDir))
        size = first_im.size
        width = size[0] * 2
        height = size[1]
        size = width, height
        single_im = Image.new('RGB', size, 'white')
        single_im.paste(first_im, (0, 0))
        single_im.paste(second_im, (820, 0))
        box = (0, 0, 1640, 1076)
        cropped = single_im.crop(box)
        cropped.save('/Novus_flask/downloaded/{}/page1.jpeg'.format(workingDir))
        unlink('/Novus_flask/downloaded/{}/page0.jpeg'.format(workingDir))

        # Crop page1
        original = Image.open('/Novus_flask/downloaded/{}/page1.jpeg'.format(workingDir))
        left = 410
        top = 0
        right = 1230
        bottom = 1076
        cropped = original.crop((left, top, right, bottom))
        cropped.save('/Novus_flask/downloaded/{}/page1.jpeg'.format(workingDir))

    except Exception as e:
        print(e)
        yield 'ERROR: ' + str(e) + '<br/>\n'
        yield 'click on the back button and retry...<br/>\n'

    # post start/end time for analysis
    end_time = str(datetime.now().strftime('%d-%m-%Y__%H:%M:%S'))
    yield 'end time:' + end_time + '<br/>\n'


    # zip the files and remove the jpeg images
    try:
        working_dir = '/Novus_flask/downloaded/' + start_time + '/'
        zip_dir = '/Novus_flask/zip/'
        with ZipFile(zip_dir + start_time + '.zip', mode='a') as zf:
            for f in listdir(working_dir):
                zf.write(working_dir + f, basename(f))
        move(zip_dir + start_time + '.zip', working_dir + start_time + '.zip')
        yield 'file zipped'
        for f in listdir(working_dir):
            if 'jpeg' in f:
                unlink(working_dir + f)
    except Exception as e:
        yield str(e)

    yield '<br/>\n'
    yield 'complete'
    yield '<br/>\n'
    yield '<a href=ftp://flask.novusgroup.co.za:2121/{}/{}>' \
          'download file</a>'.format(start_time, start_time + '.zip')


'''
if __name__ == '__main__':
    start_scrape('RooiRose', 'https://www.magzter.com/reader/3889/310709', 3)
'''